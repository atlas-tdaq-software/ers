# Error Reporting Service (ERS)

The Error Reporting System (ERS) software package provides a common API for error reporting
in the ATLAS TDAQ system. ERS offers several macro that can be used for reporting pre-defined 
errors if some conditions are violated. ERS also provides tools for defining custom classes that 
can be used for reporting specific issues.

## Header File
In order to use ERS functionality an application has to include a single header file **ers/ers.h**

## Assertion Macro
ERS provides several convenience macro for checking conditions in C++ code. If a certain condition
is violated these macro create a new instance of **ers::Assertion** class and send it to
the **ers::fatal** stream. Further processing depends on the **ers::fatal** stream configuration.
By default the issue is just printed to the standard error stream.

Here is a list of available macro:
 * **ERS_ASSERT( expression )** generic macro that checks whether a given expression is valid.
 * **ERS_PRECONDITION( expression )** the same as ERS_ASSERT but uses a user defined message to be reported.
 * **ERS_RANGE_CHECK( min, val, max )** is a special type of pre-condition, which checks that a value
is in a closed range between **min** and **max** values. 
 * **ERS_STRICT_RANGE_CHECK( min, val, max )** is similar to the ERS_RANGE_CHECK
but does not allow the checked value to be equal to either **min** or **max** values. 

## Logging Macro
ERS package provides several macro for information logging:
 * **ERS_DEBUG( level, message )** - creates a new issue of ers::Message type and sends it to the **ers::debug** stream
 * **ERS_LOG( message )** - creates a new issue of ers::Message type and sends it to the **ers::log** stream
 * **ERS_INFO( message )** - creates a new issue of ers::Message type and sends it to the **ers::info** stream
 
> **Note:** ERS_DEBUG macro is defined to empty statement if **ERS_NO_DEBUG** macro is defined at compilation time.

Each of these macro constructs an new object of ers::Message type and sends it to the appropriate stream.
The **message** argument the macro can be any value for which the standard C++ output stream operator
(**operator<<**) is defined. This means that the message can be a single value of a certain type as well
as any sequence of output operations, e.g.:

~~~cpp
ERS_DEBUG( 1, "simple debug output " << 123 << " that shows how to use debug macro" )
~~~

The actual behavior of these macro depends on the configuration of the corresponding ERS streams.
Debug macro can be disabled at run-time by defining the **TDAQ_ERS_DEBUG_LEVEL** environment 
variable to the highest possible debug level.
For instance, if **TDAQ_ERS_DEBUG_LEVEL** is set to N, then **ERS_DEBUG( M, ... )** where **M > N**
will not produce any output. The default value for the **TDAQ_ERS_DEBUG_LEVEL** is 0. Negative
debug levels are also allowed.

The amount of information, which is printed for the issue depends on the current ERS verbosity level,
which can be controlled via the **TDAQ_ERS_VERBOSITY_LEVEL** environment variable.

~~~
export TDAQ_ERS_VERBOSITY_LEVEL=N
~~~

where N must be an integer number.

By default the verbosity level is zero. 
 * For N = 0 the following information is reported for the current issue:
    * severity name (DEBUG, LOG, INFO, WARNING, ERROR, FATAL)
    * the time of the issue occurrence
    * the issue's context, which consists of the package name, the file name, the function name and the line number where the issue has occurred
    * the issue's message
 * For N > 0 the issue attributes names and values are reported in addition to 0-level data
 * For N > 1 the following information is added to the issue:
    * host name
    * user name
    * process id
    * process current working directory
 * For N > 2 the stack trace of the issue is printed if the current binary was compiled without **ERS_NO_DEBUG** macro.
 
> **Note:** If **ERS_NO_DEBUG** macro was defined at compilation time no stack trace information is added to the ERS issues.

## Using Custom Issue Classes
ERS assumes that user functions should throw exceptions in case of errors. If such exceptions
are instances of a class that inherits the **ers::Issue** one, ERS offers a number of advantages with 
respect to handling of such exceptions:
 * Such exceptions can be directly sent to any ERS stream
 * One can create chains of exceptions to preserve the original cause of the problem as well as the error handling sequence
 * These exceptions can be printed to the standard C++ output streams

In order to define a custom issue one has to do the following steps:
 * Declare a class, which inherits **ers::Issue**
 * Implement 3 pure virtual functions, declared in the ers:Issue class
 * Register the new class using the **ers::IssueFactory::register_issue** function

ERS defines two helper macro, to simplify these steps. The macro are called **ERS_DECLARE_ISSUE**
and **ERS_DECLARE_ISSUE_BASE**. The first one should be used to declare an issue class that inherits
from the **ers::Issue** as it is shown on the following example:

~~~cpp
ERS_DECLARE_ISSUE(
    ers,                                                          // namespace
    Assertion,                                                    // issue name
    "Assertion (" << condition << ") failed because " << reason,  // message
    ((const char *)condition )                                    // first attribute
    ((const char *)reason )                                       // second attribute
)
~~~

This macro can be used to declare arbitrary number of attributes for the issue class. Note that the attributes' names may appear in the message expression. Also note a special syntax of the attributes declaration, which must always be declared using a space separated sequence of **((attribute_type)attribute_name)** tokens.
All the brackets in this expression are essential. Do not use commas to separate individual attributes.
Attributes can be of any standard or custom types for which the output operator to the standard C++ output stream and the input operator from the standard C++ input stream are defined. These operators must be implemented in a consistent way, i.e. the input operator must be able to unambiguously restore the state of the attribute from the stream,
which had been used to save the object's state with the output operator. Evidently all the built-in C++ types satisfy this criteria and therefore can be freely used for the ERS issue declaration.
For the above example the **ERS_DECLARE_ISSUE** macro generates the following C++ code:

~~~cpp
namespace ers {
    class Assertion : public ers::Issue {
        template <class> friend class ers::IssueRegistrator;
        Assertion() { ; }
        
        static const char * get_uid() { return "ers::Assertion"; }
        
        virtual void raise() const throw( std::exception ) { throw *this; }
        virtual const char * get_class_name() const { return get_uid(); }
        virtual ers::Issue * clone() const { return new ers::Assertion( *this ); }
        
    public:
        Assertion( const ers::Context & context , const char * condition , const char * reason )
            : ers::Issue( context ) {
            set_value( "condition", condition );
            set_value( "reason", reason );
            std::ostringstream out;
            out << "Assertion (" << condition << ") failed because " << reason;
            set_message( out.str() );
        }
        
        Assertion( const ers::Context & context, const std::string & msg , const char * condition , const char * reason )
            : ers::Issue( context, msg ) {
            set_value( "condition", condition );
            set_value( "reason", reason );
        }
        
        Assertion( const ers::Context & context , const char * condition , const char * reason , const std::exception & cause )
            : ers::Issue( context, cause ) {
            set_value( "condition", condition );
            set_value( "reason", reason );
            std::ostringstream out;
            out << "Assertion (" << condition << ") failed because " << reason;
            set_message( out.str() );
        }
        
        const char * get_condition () {
            const char * val;
            get_value( "condition", val );
            return val;
        }
        
        const char * get_reason () {
            const char * val;
            get_value( "reason", val );
            return val;
        }
    };
}
~~~


The macro **ERS_DECLARE_ISSUE_BASE** has to be used to declare a new issue class,
which inherits from one of the existing custom ERS issue classes. For example, the following class
inherits from the **ers::Assertion** class defined above:

~~~cpp
ERS_DECLARE_ISSUE_BASE(
   ers,                                                          // namespace name
   Precondition,                                                 // issue name
   ers::Assertion,                                               // base issue name
   "Precondition (" << condition << ") located in " << location
                     << " failed because " << reason,            // message
   ((const char *)condition ) ((const char *)reason ),           // base class attributes
   ((const char *)location )                                     // this class attributes
)
~~~

This declaration will generate the following code at compilation time:

~~~cpp
namespace ers {
    class Precondition : public ers::Assertion {
        template <class> friend class ers::IssueRegistrator;
        Precondition() { ; }
        
        static const bool registered = ers::IssueRegistrator< ers::Precondition >::instance.done;
        static const char * get_uid() { return "ers::Precondition"; }
        
        virtual void raise() const throw( std::exception ) { throw *this; }
        virtual const char * get_class_name() const { return get_uid(); }
        virtual ers::Issue * clone() const { return new ers::Precondition( *this ); }
        
    public:
        Precondition( const ers::Context & context , const char * condition , const char * reason, const char * location )
            : ers::Assertion( context, condition, reason ) {
                set_value( "location", location );
            std::ostringstream out;
            out << "Precondition (" << condition << ") located in " << location << ") failed because " << reason;
            set_message( out.str() );
        }
        
        Precondition( const ers::Context & context, const std::string & msg , const char * condition , const char * reason, const char * location )
            : ers::Assertion( context, msg, condition, reason ) {
            set_value( "location", location );
        }
        
        Precondition( const ers::Context & context , const char * condition , const char * reason , const char * location, const std::exception & cause )
            : ers::Assertion( context, condition, reason, cause ) {
            set_value( "location", location );
            std::ostringstream out;
            out << "Precondition (" << condition << ") located in " << location << ") failed because " << reason;
            set_message( out.str() );
        }
        
        const char * get_location () {
            const char * val;
            get_value( "location", val );
            return val;
        }
    };
}
~~~

## ERS_HERE macro
ERS defines a special convenience macro called **ERS_HERE** that is used to add the context information, like the file name, the line number and the signature of the function where the issue was constructed, to the new issue object.
When a new issue is created one must always use **ERS_HERE** macro as the first parameter of the issue constructor.

## Exception Handling
The following example shows how to handle exceptions, which were thrown using ERS issue classes.

~~~cpp
#include <ers/SampleIssues.h>

    try {
        foo( );
    }
    catch ( ers::PermissionDenied & ex ) {
        ers::CantOpenFile issue( ERS_HERE, ex.get_file_name(), ex );
        ers::warning( issue );
    }
    catch ( ers::FileDoesNotExist & ex ) {
        ers::CantOpenFile issue( ERS_HERE, ex.get_file_name(), ex );
        ers::warning( issue );
    }
    catch ( ers::Issue & ex ) {
        ERS_DEBUG( 0, "Unknown issue caught: " << ex )
        ers::error( ex );
    }
    catch ( std::exception & ex ) {
        ers::CantOpenFile issue( ERS_HERE, "unknown", ex );
        ers::warning( issue );
    }
~~~

This example demonstrates the main features of using ERS issues for exception handling:
 * A newly created issue always has the ERROR severity by default. When the issue is reported to an ERS stream its severity changes to the one of this stream.
 * An issue can be send to one of the existing ERS streams using one of the following functions:
**ers::debug**, **ers::info**, **ers::warning**, **ers::error**, **ers::fatal**
 * Any ERS issue has a constructor, which accepts another issue as its last parameter. If this
constructor is used the new issue will hold the copy of the original one and will report it as its cause.
 * Any ERS issue has a constructor, which accepts **std::exception** issue as its last parameter.
    If it is used the new issue will hold the copy of the given **std::exception** and will report it as its cause.

## ERS Streams
The ERS API provides several functions, one per severity level, to report issues.
The issues which are reported to the ERS streams may be forwarded to different destinations depending on the current streams configuration. By default the ERS streams are configured in the following way:
 * **ers::debug()** - "lstdout" - prints issues to the standard C++ output stream
 * **ers::log()** - "lstdout" - prints issues to the standard C++ output stream
 * **ers::info()** - "throttle,lstdout" - prints throttled issues to the standard C++ output stream
 * **ers::warning()** - "throttle,lstderr" - prints throttled issues to the standard C++ error stream
 * **ers::error()** - "throttle,lstderr" - prints throttled issues to the standard C++ error stream
 * **ers::fatal()** - "lstderr" - prints issues to the standard C++ error stream

> **Note:** the letter "l" at the beginning of "lstdout" and "lstderr" names indicates that these stream
> implementations are thread-safe and can be safely used in multi-threaded applications so that
> issues reported from different threads will not interfere.

In order to change the default configuration for an ERS stream one should use
the **TDAQ_ERS_<SEVERITY>** environment variable. For example the following command:

~~~
export TDAQ_ERS_ERROR="lstderr,throw" 
~~~

will cause all the issues, which are reported via the **ers::error()** function, been printed to 
the standard C++ error stream and then been thrown using the C++ **throw** operator.

ERS also provides a simple filtering stream implementation that can be used in the following way:

~~~ 
export TDAQ_ERS_ERROR="lstderr,filter(foo,bar),throw" 
~~~

The difference with the previous configuration is that only errors, which have either **foo** or **bar** qualifiers
will be passed to the **throw** stream. Users can add any qualifiers to their specific issues
by using the **Issue::add_qualifier** function. By default every issue has one qualifier associated
with it - the name of the TDAQ software package, which has been used to build the binary (the library or the application)
where the issue is constructed.

One can also define reversed filters like in the following example:

~~~
> export TDAQ_ERS_ERROR="lstderr,filter(!foo,!bar),throw"
~~~

This configuration will throw only the issues, which come neither from **foo** nor from **bar** TDAQ packages.

### Existing Stream Implementations
ERS provides several stream implementations which can be used in any combination in ERS streams configurations.
Here is the list of available stream implementations:
 * **stdout** - prints issues to the standard C++ output stream. It is not thread-safe.
 * **stderr** - prints issues to the standard C++ error stream. It is not thread-safe.
 * **lstdout** - prints issues to the standard C++ output stream. It is thread-safe.
 * **lstderr** - prints issues to the standard C++ error stream. It is thread-safe.
 * **lock** - locks a global mutex for the duration of reporting an issue to the next streams in the given configuration.
This stream can be used for adding thread-safety to an arbitrary non-thread-safe stream implementation. For example
**lock,stdout** configuration is equivalent to **lstdout**.
 * **null** - silently drop any reported issue.
 * **throw** - apply the C++ throw operator to the reported issue
 * **abort** - calls abort() function for any issue reported
 * **exit** - calls exit() function for any issue reported
 * **filter(A,B,!C,...)** - pass through only issues, which have either A or B and don't have C qualifier
 * **rfilter(RA,RB,!RC,...)** - the same as "filter" stream but treats all the given parameters as regular expressions.
 * **throttle(initial_threshold, time_interval)** - rejects the same issues reported within the **time_interval** after
passing through the **initial_threshold** number of them.

## Custom Stream Implementation
While ERS provides a set of basic stream implementations one can also implement a custom one if this is required.
Custom streams can be plugged into any existing application which is using ERS without recompiling this application.

### Implementing a Custom Stream
In order to provide a new custom stream implementation one has to declare a sub-class of the **ers::OutputStream**
class and implement its pure virtual method called **write**. Here is an example of how this is done by the 
**FilterStream** stream implementation:

~~~cpp
void
ers::FilterStream::write( const ers::Issue & issue )
{
    if ( is_accepted( issue ) ) {
        chained().write( issue );
    }
}
~~~

An implementation of the **ers::OutputStream::write** function must decide whether to pass the given
issue to the next stream in the chain or not. If a custom stream does not provide any filtering
functionality then it shall always pass the input message to the next stream by using the
**chained().write( issue )** code.

### Registering a Custom Stream
In order to register and use a custom ERS stream implementation one must use a dedicated macro called
**ERS_REGISTER_STREAM** in the following way:

~~~
ERS_REGISTER_OUTPUT_STREAM( ers::FilterStream, "filter", format )
~~~

The first parameter of this macro is the name of the class which implements the new stream; the second one
gives a new stream name to be used in ERS stream configurations (this is the name which one can put to the
**TDAQ_ERS_<SEVERITY>** environment variables); and the last parameter is a placeholder for the stream class
constructor parameters. If the constructor of the new custom stream does not require parameters then the last
field of this macro should be left empty.

### Using Custom Stream
In order to use a custom stream one has to build a shared library from the class that implements this stream
and pass this library to ERS by setting its name to the **TDAQ_ERS_STREAM_LIBS** environment variable.
For example if this macro is set to the following value:

~~~
export  TDAQ_ERS_STREAM_LIBS=MyCustomFilter
~~~

then ERS will try to load dynamically the **libMyCustomFilter.so** library if it appears in one of the directories listed in the **LD_LIBRARY_PATH** environment variable.

## ERS Thread Safety
ERS is fully thread-safe and can be seamlessly used for error reporting in multi-threaded applications.

## ERS Issue Catcher
ERS provides a way of intercepting issues originated from any library linked with the current process by installing a so called issue catcher by using the **ers::set_issue_catcher** function. If this was done then any ERS issue that is reported via the **ers::error()**, **ers::fatal()** or **ers::warning()** functions will be passed to the issue catcher thread which will be responsible for further handling of this issue.

### Installing the Error Catcher
The error catcher should be installed by calling the **ers::set_issue_catcher** function and passing
it a function object as parameter. This function object will be executed in the context of a dedicated
thread (created by the **ers::set_issue_catcher** function) for every issue which is reported by the current
application via **ers::error()**, **ers::fatal()** or **ers::warning()** functions.
The parameter of the **ers::set_issue_catcher** must be any callable object that can be converted to the **std::function<void( const ers::Issue & )>** type which allows to use as a plain C-style function as well as a C++ member function to implement the error catcher. For example one can implement the issue catcher in the following way:

~~~cpp
struct IssueCatcher {
   void handler( const ers::Issue & issue ) {
      std::cout << "IssueCatcher has been called: " << issue << std::endl;
   }
};
~~~

which then can be registered with ERS in the following way:

~~~cpp
IssueCatcher * catcher = new IssueCatcher();
ers::IssueCatcherHandler * handler;
try {
    handler = ers::set_issue_catcher( std::bind( &IssueCatcher::handler, catcher, std::placeholders::_1 ) );
}
catch(ers::IssueCatcherAlreadySet & ex){
    ...
}
~~~

Note that the issue catcher can be set only once for the lifetime of application. An attempt to set it again will fail and the **ers::IssueCatcherAlreadySet** exception will be thrown.

To unregister the previously installed issue catcher one needs to destroy the handler that is returned by
the **ers::set_issue_catcher** function using the C++ **delete** operator:

~~~cpp
delete handler;
~~~

## Receiving Issues Across Application Boundaries
There is a specific implementation of ERS input and output streams which allows to exchange issue
across application boundaries, i.e. to allow a process to receive ERS issues produces by the other ones.
The following example shows how to use this facility:

~~~cpp
#include <ers/InputStream.h>
#include <ers/ers.h>

struct MyIssueReceiver : public ers::IssueReceiver {
    void receive( const ers::Issue & issue ) {
        std::cout << issue << std::endl;
    }
};

MyIssueReceiver * receiver = new MyIssueReceiver;
try {
    ers::StreamManager::instance().add_receiver( "mts", "*", receiver );
}
catch( ers::Issue & ex ) {
    ers::fatal( ex );
}
~~~

The **MyIssueReceiver** instance will be getting all messages, which are sent to the **mts** stream implementation
by all applications working in the current TDAQ partition whose name will be taken from the **TDAQ_PARTITION** 
environment variable. Alternatively one may pass partition name explicitly via the **mts** stream parameters list:

~~~cpp
MyIssueReceiver * receiver = new MyIssueReceiver;
try {
    ers::StreamManager::instance().add_receiver( "mts", {"my partition name", "*"}, receiver );
}
catch( ers::Issue & ex ) {
    ers::fatal( ex );
}
~~~

To cancel a previously made subscription one should use the **ers::StreamManager::remove_receiver** function 
by giving it a pointer to the corresponding receiver object, e.g.:

~~~cpp
try {
    ers::StreamManager::instance().remove_receiver( receiver );
}
catch( ers::Issue & ex ) {
    ers::error( ex );
}
~~~

